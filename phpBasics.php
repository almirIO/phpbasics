<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP Basics</title>
</head>
<body>

<h1> PHP basics </h1>

<?php $myName = "Almir";?>
<?php $number1 = 5; ?>
<?php $number2 = 8; ?>
<?php $total = $number1 + $number2; ?>



<h2>Here is my name being echod from a php variable: <?php echo $myName ?></h2>

<h3>Number 1: <?php echo $number1 ?></h3>
<h3>Number 2: <?php echo $number2 ?></h3>
<h3>Total when added: <?php echo $total ?></h3>

<h3 id="displayClasses">
<?php 
   echo '<script>var classes=["PHP","HTML","Javascript"]</script>';
   echo '<script>document.getElementById("displayClasses").innerHTML= classes </script>';
 ?>
</h3>

</body>
</html>